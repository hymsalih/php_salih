<?php

namespace App\Http\Controllers;

use App\Http\Requests\SalesTeamCreateRequest;
use App\Http\Requests\SalesTeamUpdateRequest;
use App\Repository\SalesTeamsInterface;
use Illuminate\Http\Request;

class SalesTeamController extends Controller
{
    protected $salesTeam;

    /**
     * SalesTeamController constructor.
     * @param SalesTeamsInterface $salesTeam
     * load the sales team interface
     */
    public function __construct(SalesTeamsInterface $salesTeam)
    {
        $this->middleware('auth');
        $this->salesTeam = $salesTeam;
    }

    /**
     * Display a all the sales team data
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \view('sales-team.index', [
            'salesTeams' => $this->salesTeam->getAllData()
        ]);
    }

    /**
     * Show the form to create new sales team
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \view('sales-team.create');
    }

    /**
     * Store a newly created sales team in th in the DB table.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalesTeamCreateRequest $request)
    {
        $validated = $request->validated();
        $validated['comments'] = $request->comments;
        $this->salesTeam->store($validated);
        return redirect()->route("sales-team.index");
    }

    /**
     * Display the sales team member by id
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->salesTeam->view($id);
        if ($data)
            return response()->json($data);
    }

    /**
     * Show the form to edit the sales team member
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salesTeam = $this->salesTeam->view($id);
        if ($salesTeam)
            return \view('sales-team.edit', [
                'team' => $salesTeam
            ]);


    }

    /**
     * Update the specified sales member in the DB table
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SalesTeamUpdateRequest $request, $id)
    {
        $validated = $request->validated();
        $validated['comments'] = $request->comments;
        $this->salesTeam->update($id, $validated);
        return redirect()->route("sales-team.index");

    }

    /**
     * Remove the specified sales member from DB table.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->salesTeam->delete($id);
    }
}
