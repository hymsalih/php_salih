@extends('layouts.app.layout')
@section('content')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Sales Team</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href={{route("sales-team.index")}}#">Sales Team</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add New Sale Team</h3>
                            <br>
                        </div>
                        <form name="add-sales-team" method="post" action="{{route('sales-team.store')}}">
                            <div class="card-body">
                                @if($errors->any())
                                    <div class="alert alert-danger alert-dismissible">
                                        <h5>Error!</h5>
                                        @if($errors->any())
                                            {!! implode('', $errors->all('<div>:message</div>')) !!}
                                        @endif
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control"
                                           placeholder="" name="full_name" value="{{old('full_name')}}">
                                </div>
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" class="form-control"
                                           name="email_address" value="{{old('email_address')}}">
                                </div>
                                <div class="form-group">
                                    <label>Telephone Number</label>
                                    <input type="text" class="form-control" name="telephone_number"
                                           value="{{old('telephone_number')}}">
                                </div>
                                <div class="form-group">
                                    <label>Current Working Route</label>
                                    <input type="text" class="form-control" name="current_working_route"
                                           value="{{old('current_working_route')}}">
                                </div>
                                <div class="form-group">
                                    <label>Comments</label>
                                    <textarea class="form-control" name="comments">{{old('comments')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Joined Date</label>
                                    <input type="text" class="form-control" placeholder="yyyy-mm-dd" name="joined_date"
                                           value="{{old('joined_date')}}">
                                </div>
                            </div>
                            <div class="card-footer">
                                @csrf
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection
