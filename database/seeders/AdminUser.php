<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Hash;
class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exists = User::where('email', 'admin@eyepax.com')->count();
        if (!$exists)
            User::insert([
                'name' => 'Admin',
                'email' => 'admin@eyepax.com',
                'password' => Hash::make('password'),
            ]);
    }
}
