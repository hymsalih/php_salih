<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesTeamCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'email_address' => 'required|email|unique:sales_teams',
            'telephone_number' => 'required|unique:sales_teams|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'current_working_route' => 'required',
            'joined_date' => 'required|date_format:Y-m-d',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'email' => 'trim|lowercase',
        ];
    }
}
