<?php

namespace App\Repository;

use App\Models\SalesTeam;

interface SalesTeamsInterface
{
    public function getAllData();

    public function store($data);

    public function view($id);

    public function delete($id);

    public function update($id, $data);
}
