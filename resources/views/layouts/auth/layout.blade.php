<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EyePax | Login</title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{url("css/all.min.css")}}">
    <link rel="stylesheet" href="{{url('/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{url('/css/tempusdominus-bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{url("/css/icheck-bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{url('/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{url('/css/OverlayScrollbars.min.css')}}">
    <link rel="stylesheet" href="{{url('/css/daterangepicker.css')}}">
</head>
<body class="hold-transition login-page">
@yield('content')
</body>
</html>
