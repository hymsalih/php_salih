<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SalesTeamUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->route()->parameter('sales_team');
        return [
            'full_name' => 'required',
            'email_address' => ['required', Rule::unique('sales_teams', 'email_address')->ignore($id)],
            'telephone_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10', 'max:10', Rule::unique('sales_teams', 'telephone_number')->ignore($id)],
            'current_working_route' => 'required',
            'joined_date' => 'required|date_format:Y-m-d',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'email' => 'trim|lowercase',
        ];
    }
}
