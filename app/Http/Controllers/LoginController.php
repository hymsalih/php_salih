<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminLoginRequest;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * show the user login form
     */
    public function index()
    {
        return view('login.frm-login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * logout function to redirect logout users to login page
     */

    public function logout()
    {
        Auth::logout();
        return redirect()->to("/login");
    }

    /**
     * @param AdminLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * function to handle admin login request once login pass it will redirect to sales team view page
     */

    public function doLogin(AdminLoginRequest $request)
    {
        $validated = $request->validated();
        $remember = ($request->remember) ? true : false;
        if (!Auth::attempt(array('email' => strtolower($request->email), 'password' => $request->password), $remember)) {
            return redirect()->back()->withErrors(['msg' => 'Invalid email address or password!']);
        }
        return redirect()->to(route("sales-team.index"));
    }
}
