<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class ApplicationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function login_form_input_validation()
    {
        Event::fake();
        $response = $this->json('POST', route('login'), [
            'email' => 'test@gmail.com',
            'password' => 'password',
        ]);
        $response->assertRedirect("/");
    }

    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function sales_team_add_form_validation()
    {
        Event::fake();
        $response = $this->actingAs(User::factory()->create())
            ->json('POST', route('sales-team.store'), [
                'full_name' => 'test',
                'email_address' => 'test@gmail.com',
                'telephone_number' => '1234567890',
                'current_working_route' => 'test',
                'joined_date' => '2021-12-27',
            ]);
        $response->assertRedirect(route('sales-team.index'));
    }

    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function sales_team_update_form_validation()
    {
        Event::fake();
        $this->withoutExceptionHandling();
        $response = $this->actingAs(User::factory()->create())
            ->json('PUT', route('sales-team.update', 1), [
                'full_name' => 'test',
                'email_address' => 'test@gmail.com',
                'telephone_number' => '1234567890',
                'current_working_route' => 'test',
                'joined_date' => '2021-12-27',
            ]);
        $response->assertRedirect(route('sales-team.index'));
    }

    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function sales_team_delete_form_validation()
    {
        Event::fake();
        $this->withoutExceptionHandling();
        $response = $this->actingAs(User::factory()->create())
            ->json('DELETE', route('sales-team.destroy', 1));
        $response->assertOk();
    }


    /** @test */
    public function sales_manager_login_url()
    {
        $response = $this->get(route('login'));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @test
     */
    public function sales_manager_logout_url()
    {
        $response = $this->get(route('logout'));
        $response->assertRedirect(route("login"));
    }

    /**
     * @test
     */
    public function sales_manage_can_see_sale_team_lists()
    {
        $this->actingAs(User::factory()->create());
        $this->get(route("sales-team.index"))->assertOk();
    }

    /**
     * @test
     */
    public function sales_manage_can_add_new_sales_team()
    {
        $this->actingAs(User::factory()->create());
        $this->get(route("sales-team.create"))->assertOk();
    }

    /**
     * @test
     */
    public function sales_manage_can_view_sales_team()
    {
        $this->actingAs(User::factory()->create());
        $this->get(route("sales-team.index", 1))->assertOk();
    }


    /**
     * @test
     */
    public function sales_manage_can_edit_sales_team()
    {
        $this->actingAs(User::factory()->create());
        $this->get(route("sales-team.edit", 1))->assertOk();
    }

    /**
     * @test
     */
    public function sales_manage_can_show_sales_team()
    {
        $this->actingAs(User::factory()->create());
        $this->get(route("sales-team.show", 1))->assertOk();
    }
}
