<?php

namespace App\Providers;
use App\Repository\SalesTeamsInterface;
use App\Repository\SalesTeamsRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SalesTeamsInterface::class,SalesTeamsRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
