@extends('layouts.app.layout')
@section('content')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Sales Team</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Sales Team</li>
            </ol>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-header text-right">
                            <a href="{{route("sales-team.create")}}" class="btn btn-primary btn-sm">Add New</a>
                        </div>
                        <div class="card-body">
                            <div id="" class="dataTables_wrapper dt-bootstrap4">
                                <table id="example1"
                                       class="table table-bordered table-striped dataTable dtr-inline"
                                       role="grid" aria-describedby="example1_info">
                                    <tbody>
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Full Name</th>
                                        <th>Email Address</th>
                                        <th>Telephone Number</th>
                                        <th>Route</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    @forelse($salesTeams as $key => $team)
                                        <tr>
                                            <td>{{$team->id}}</td>
                                            <td>{{$team->full_name}}</td>
                                            <td>{{$team->email_address}}</td>
                                            <td>{{$team->telephone_number}}</td>
                                            <td>{{$team->current_working_route}}</td>
                                            <td>
                                                <a href="#"
                                                   class="btn btn-sm btn-primary view-team"
                                                   data-id="{{$team->id}}">View</a>

                                                <a href="{{route('sales-team.edit',$team->id)}}"
                                                   class="btn btn-sm btn-info">Edit</a>

                                                <a href="#"
                                                   class="btn btn-sm btn-danger delete-team" data-id="{{$team->id}}"
                                                   data-name="{{$team->full_name}}">Delete</a>

                                            </td>
                                        </tr>
                                        @empty
                                        @endforelse
                                        </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <tr>
                            <td>ID</td>
                            <td class="view-id"></td>
                        </tr>
                        <tr>
                            <td>Full Name</td>
                            <td class="view-fname"></td>
                        </tr>
                        <tr>
                            <td>Email Address</td>
                            <td class="view-email"></td>
                        </tr>
                        <tr>
                            <td>Telephone</td>
                            <td class="view-telephone"></td>
                        </tr>

                        <tr>
                            <td>Joined Date</td>
                            <td class="view-joindate"></td>
                        </tr>

                        <tr>
                            <td>Current Route</td>
                            <td class="view-route"></td>
                        </tr>

                        <tr>
                            <td>Comments</td>
                            <td class="view-comments"></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        /**
         * Ajax call to delete the sales member
         */
        $(document).on("click", ".delete-team", function () {
            var full_name = $(this).attr("data-name");
            var result = confirm("Want to delete " + full_name + "?");
            if (result) {
                var id = $(this).data("id");
                var token = $("meta[name='csrf-token']").attr("content");
                $.ajax(
                    {
                        url: "sales-team/" + id,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function () {
                            window.location = "{{route('sales-team.index')}}"
                        }
                    });

            }
        });

        /**
         * Ajax call to get sale member data to the pop up view
         */
        $(document).on("click", ".view-team", function () {
            var id = $(this).data("id");
            var token = $("meta[name='csrf-token']").attr("content");
            $.ajax(
                {
                    url: "sales-team/" + id,
                    type: 'GET',
                    data: {
                        "id": id,
                        "_token": token,
                    },
                    success: function (response) {
                        $(".modal-title").text(response.full_name)
                        $("td.view-id").text(response.id)
                        $("td.view-fname").text(response.full_name)
                        $("td.view-email").text(response.email_address)
                        $("td.view-telephone").text(response.telephone_number)
                        $("td.view-joindate").text(response.joined_date)
                        $("td.view-route").text(response.current_working_route)
                        $("td.view-comments").text(response.comments)
                        $("#modal-default").modal('show');
                    }
                });


        });
    </script>
@endpush


