@extends('layouts.auth.layout')
@section('content')
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                @if($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                        <h5>Alert!</h5>
                        {{$errors->first()}}
                    </div>
                @endif

                <p class="login-box-msg">Admin Login</p>
                <form action="{{route("login")}}" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                               placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputEmail1"
                               placeholder="Enter password">
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                            <input type="checkbox" id="remember" name="remember">
                                <label for="remember">
                                   Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-4">
                            @csrf
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
