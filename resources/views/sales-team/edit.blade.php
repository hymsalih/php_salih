@extends('layouts.app.layout')
@section('content')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Sales Team</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href={{route("sales-team.index")}}#">Sales Team</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Update Sale Team</h3>
                            <br>
                        </div>
                        <form method="POST" action="{{route('sales-team.update', ['sales_team' => $team->id])}}">
                            <div class="card-body">
                                @if($errors->any())
                                    <div class="alert alert-danger alert-dismissible">
                                        <h5>Error!</h5>
                                        @if($errors->any())
                                            {!! implode('', $errors->all('<div>:message</div>')) !!}
                                        @endif
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control"
                                           placeholder="" name="full_name" value="{{$team->full_name}}">
                                </div>
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" class="form-control"
                                           name="email_address" value="{{$team->email_address}}">
                                </div>
                                <div class="form-group">
                                    <label>Telephone Number</label>
                                    <input type="text" class="form-control" name="telephone_number"
                                           value=" {{$team->telephone_number}}">
                                </div>
                                <div class="form-group">
                                    <label>Current Working Route</label>
                                    <input type="text" class="form-control" name="current_working_route"
                                           value="{{$team->current_working_route}}">
                                </div>
                                <div class="form-group">
                                    <label>Comments</label>
                                    <textarea class="form-control" name="comments">{{$team->comments}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Joined Date</label>
                                    <input type="text" class="form-control" placeholder="yyyy-mm-dd" name="joined_date"
                                           value="{{$team->joined_date}}">
                                </div>
                            </div>
                            <div class="card-footer">
                                @csrf
                                {{ method_field('PUT') }}
                                <input type="hidden" name="id" value="{{$team->id}}">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection
