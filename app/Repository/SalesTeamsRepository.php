<?php


namespace App\Repository;

use App\Models\SalesTeam;

class SalesTeamsRepository implements SalesTeamsInterface
{
    /**
     * @return retrun all the sales member
     */
    public function getAllData()
    {
        return SalesTeam::latest()->get();
    }

    /**
     * @param $data input form data
     * @return bool
     * store sale member data to the sales_member table
     */

    public function store($data)
    {
        $salesTeam = new SalesTeam();
        $salesTeam->full_name = $data['full_name'];
        $salesTeam->email_address = $data['email_address'];
        $salesTeam->telephone_number = $data['telephone_number'];
        $salesTeam->current_working_route = $data['current_working_route'];
        $salesTeam->joined_date = $data['joined_date'];
        $salesTeam->comments = $data['comments'];
        return $salesTeam->save();
    }

    /**
     * @param $id sales member id
     * @param $data sales member updating data
     * @return bool
     * update sale member data
     */
    public function update($id, $data)
    {
        $salesTeam = SalesTeam::find($id);
        if ($salesTeam) {
            $salesTeam->full_name = $data['full_name'];
            $salesTeam->email_address = $data['email_address'];
            $salesTeam->telephone_number = $data['telephone_number'];
            $salesTeam->current_working_route = $data['current_working_route'];
            $salesTeam->joined_date = $data['joined_date'];
            $salesTeam->comments = $data['comments'];
            return $salesTeam->save();
        }
    }

    /**
     * @param $id sales member id
     * @return string specific sales member data
     * show the specific sales member
     */
    public function view($id)
    {
        $saleTeam = SalesTeam::where('id', $id)->first();
        if ($saleTeam != null) {
            return $saleTeam;
        }
        return "";
    }

    /**
     * @param $id sales member id
     * @return bool
     * delete the specific sale member from table
     */

    public function delete($id)
    {
        $saleTeam = SalesTeam::where('id', $id)->first();
        if ($saleTeam != null) {
            return $saleTeam->delete();
        }
    }
}
