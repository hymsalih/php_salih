<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesTeam extends Model
{
    use HasFactory;
    protected $table = 'sales_teams';
    protected $primaryKey = 'id';
    protected $fillable = ['full_name', 'email_address', 'telephone_number', 'current_working_route', 'comments', 'joined_date'];

}
