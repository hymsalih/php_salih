
## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Sales Team Manager
This the web based application to mamange the sales memebers data. core feature of this application is Admin can view/edit/add/delete Sales team memebers.

## Setup

NOTE This application developed using Laravel 8.54 and above and It's required 7.3 or 8.0. So skip these Setup instructions if you're using Laravel 8.54 and above.

### Installation

Clone the application from repository to your local matchine
```sh
git clone https://hymsalih@bitbucket.org/hymsalih/php_salih.git
```

```sh
cd php_salih
```

Install laravel package lists
```sh
composer install
```


### Configuration
Create environment file, rename .env.example to.env file and add your database connection

```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=DB_NAME
DB_USERNAME=DB_USER_NAME
DB_PASSWORD=DB_PASSWORD
```

NOTE:- Check your local database connections. if it's not connected please turn on your local matchine databse engine.

Generate laravel key
```sh
php artisan key:generate
```


Run artisan command to load defult tables

```sh
php artisan migrate
```
After that create default admin user creadentials using laravel db:seeder
Open the project console prompt and type below artisan command and hit enter

```sh
php artisan db:seed --class=AdminUser
```
Once the above processes are successfully done start-server to run the application 

```sh
php artisan serve
```

### Application routes and User Credential
Admin login route - http://127.0.0.1:8000/login
default user name and password
```sh
admin@eyepax.com
password
```
#### PHP Unit Test
Note:- You should have sqlite module enabled in PHP.ini to execute the PHP unit test. Run below command to run application PHP unit tests
```sh
 ./vendor/bin/phpunit
```
#### Application test outputs
```sh
{
   "version":1,
   "defects":[
      
   ],
   "times":{
      "Tests\\Feature\\ApplicationTest::login_form_input_validation":0.534,
      "Tests\\Feature\\ApplicationTest::sales_team_add_form_validation":0.044,
      "Tests\\Feature\\ApplicationTest::sales_team_update_form_validation":0.024,
      "Tests\\Feature\\ApplicationTest::sales_team_delete_form_validation":0.021,
      "Tests\\Feature\\ApplicationTest::sales_manager_login_url":0.023,
      "Tests\\Feature\\ApplicationTest::sales_manager_logout_url":0.02,
      "Tests\\Feature\\ApplicationTest::sales_manage_can_see_sale_team_lists":0.062,
      "Tests\\Feature\\ApplicationTest::sales_manage_can_add_new_sales_team":0.024,
      "Tests\\Feature\\ApplicationTest::sales_manage_can_view_sales_team":0.025,
      "Tests\\Feature\\ApplicationTest::sales_manage_can_edit_sales_team":0.022,
      "Tests\\Feature\\ApplicationTest::sales_manage_can_show_sales_team":0.025
   }
}
```
#### Thank You!